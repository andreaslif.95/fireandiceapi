Application which uses the FIRE AND ICE API 
https://anapioficeandfire.com/
Your application needs to have the following THREE features:

- [x] It should allow me to lookup a character by number. (Display basic info for that character.)
- [x] It should then optionally allow me to display the names of all sworn members of the previous characters house (this can be a lot of characters!)
- [x] It must then lookup all povCharacters in the books published by "Bantam Books", it must then display this information in a grid/table/visually appealing way. (Which characters are point-of-view in which books?)
- [x] The solution should be consoled based :(
Submit only a link to the git repo for your solution