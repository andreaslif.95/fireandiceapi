package se.experis.com.controller;

import se.experis.com.api.APIHandler;
import se.experis.com.exceptions.HTTPException;
import se.experis.com.model.Book;
import se.experis.com.model.BooksWithCharacter;
import se.experis.com.model.House;
import se.experis.com.utils.Progresser;
import se.experis.com.view.View;
import se.experis.com.model.Character;

import java.io.IOException;

/**
 *  Class which controlls the flow of the application
 */
public class Controller {
    private View view;
    public Controller(View view) {
        this.view = view;
    }

    /***
     * Start of pplication flow. The user is prompted to make one of three choices.
     * 1. Search and character by it's id.
     * 2. View all povCharacters which are in books of a specific publisher.
     * 3. Exit the program.
     * In case of bad input the user will be forced to make a new choice.
     */
    public void start() {
        while(true) {
            switch (view.pickAnOption()) {
                case 1 -> characterById();
                case 2 -> povByPublisher();
                case 3 -> System.exit(0);
                default -> view.badInputOnlyAcceptNumbers();
            }
        }
    }

    /**
     * Used to get a characters information by it's id.
     * Will ask the user for id and continue to ask for it until it gets valid input.
     * Upon getting valid input the method uses the ApiHandler to fetch a character.
     * It will ask the user if they also want to see the sworn members.
     * If yes the method will use the APIhandler to fetch the houses of the character then all character within those house.
     *
     * Progresser is used to indicate that loading bars will be used during loading moments.
     *
     * Errors
     *      - HTTPerrors - If not statuscode 404, it will ask the user if they want to load again.
     *                   - If 404 it will send the message NOT FOUND and return to the start method.
     *      - IOexception - Will ask the user if they want to try to load again.
     *
     *      - Error while fetching sworn members will be printed after the function is done as (X errors while fetching)
     */
    public void characterById() {
        int id;
        while (true) {
            try {
                id = view.enterCharacterNumber();
                break;
            } catch (Exception e) {
                view.badInputOnlyAcceptNumbers();
            }
        }

        Character character;
        while(true) {
            //Initiates the progress bar.
            Progresser.initiateProgress(1);
            view.loadingBar("(Fetching character)");
            try {
                character = APIHandler.getCharacter(id);
                break;
            } catch (HTTPException e) {
                Progresser.stopProgress(); //Stops the progressbar.
                if (e.getStatusCode() == 404) {
                    view.notFound();
                    return;
                }
                //Ask the user if they want to try again.
                if (!view.httpErrorMesage(e.getStatusCode())) {
                    return;
                }
            } catch (IOException e) {
                Progresser.stopProgress();
                //Ask the user if they want to try again.
                if(!view.errorOccuredMessage()) {
                    return;
                }
            }
        }
        Progresser.stopProgress();
        view.printString(character.getRelevantInfo());
        StringBuilder stringBuilder = new StringBuilder();
        //Asks if the user want to see sworn enemies
        if (view.seeSwornMembers()) {
            int errorCount = 0;
            //Iterates each houseId of the character.
            for (int houseId : character.getHouseIds()) {
                House house;
                while(true) {
                    try {
                        house = APIHandler.getHouse(houseId); //fetches the house.
                        break;
                    } catch (HTTPException e) {
                        if (e.getStatusCode() == 404) {
                            view.notFound();
                            return;
                        }
                        //Ask the user if they want to try again.
                        if (!view.httpErrorMesage(e.getStatusCode())) {
                            return;
                        }
                    } catch (IOException e) {
                        //Ask the user if they want to try again.
                        if(view.errorOccuredMessage()) {
                            return;
                        }
                    }
                }
                Progresser.initiateProgress(house.getSwornMembers().length); //Initiate progress.
                view.loadingCount("(" + house.getName() + ")");
                stringBuilder.append(house.getName()).append("\n");
                //Iterates each character and fetches the name and first title from the API.
                for(String member : house.getSwornMembers()) {
                    try {
                        character = APIHandler.getCharacter(Integer.parseInt(member.substring(member.lastIndexOf("/") + 1)));
                        stringBuilder.append("\t\t").append(character.getName()).append(" ").append(character.getFirstTitle()).append("\n");
                    }catch (Exception e) {
                        errorCount++;
                    }
                    Progresser.addProgress();
                }
            }
            Progresser.stopProgress();
            view.printString(stringBuilder.toString());
            //If any errors we print it.
            if(errorCount > 0) {
                view.errorWhileFetchingCharacters(errorCount);
            }
        }
    }

    /**
     * Used to find all books which has the publisher Bantam Books.
     * Then gets all povCharacters from those books and prints them out.
     * Adds all characters to a BooksWithCharacter class which holds each character in a hashmap and then a arrayList with books is linked to it.
     * Progresser is used to indicate that loading bars will be used during loading moments.
     *
     * Errors
     *      - HTTPerrors - If not statuscode 404, it will ask the user if they want to load again.
     *                   - If 404 it will send the message NOT FOUND and return to the start method.
     *      - IOexception - Will ask the user if they want to try to load again.
     *
     *      - Error while fetching sworn members will be printed after the function is done as (X errors while fetching)
     */
    public void povByPublisher() {

        Book[] books;
        while(true) {
            Progresser.initiateProgress(1);
            view.loadingBar("(Fetching all books)");
            try {
                books = APIHandler.getAllBooks(); //Fetches all books.
                break;
            } catch (HTTPException e) {
                Progresser.stopProgress();
                if(e.getStatusCode() == 404) {
                    view.notFound();
                    return;
                }
                //Ask the user if they want to try again.
                if(!view.httpErrorMesage(e.getStatusCode())) {
                    return;
                }
            } catch (IOException e) {
                Progresser.stopProgress();
                //Ask the user if they want to try again.
                if(view.errorOccuredMessage()) {
                    return;
                }
            }
        }

        BooksWithCharacter booksWithCharacter = new BooksWithCharacter();
        Progresser.stopProgress();
        Progresser.initiateProgress(1);
        view.loadingBar("(Searching for povcharacters)");
        int errorCount = 0;
        for (Book book : books) {
            if (book.getPublisher().equals("Bantam Books")) {
                if(book.getPovCharacters().length < 1) {
                    booksWithCharacter.addBook(book.getName());
                }
                for (String character : book.getPovCharacters()) {
                    try {
                        /*
                            Adding a character to the booksWithCharacter class
                            We use the APIHandler to get a character and use a substring to extract only the id from the character since it is represented as a url.
                        */
                        booksWithCharacter.add(APIHandler.getCharacter(Integer.parseInt(character.substring(character.lastIndexOf("/")+1))).getName(), book.getName());
                    } catch (Exception e) {
                        errorCount++;
                    }
                }
            }
        }
        Progresser.stopProgress();
        view.printString(booksWithCharacter.toStringFormat());
        if(errorCount > 0) {
            view.errorWhileFetchingCharacters(errorCount);
        }
    }
}
