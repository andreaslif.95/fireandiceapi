package se.experis.com.exceptions;

public class HTTPException extends Exception {
    int statusCode;
    public HTTPException(String message, int statusCode) {
        super(message);
        this.statusCode = statusCode;
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

    public int getStatusCode() {
        return statusCode;
    }
}
