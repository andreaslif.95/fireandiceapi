package se.experis.com.utils;

/**
 * Used to help handle the loading bars.
 */
public class Progresser {
    private static int currentProgress;
    private static int maxProgress;
    private static boolean runProgress;

    /**
     * Iniates a progress.
     * @param maxProgg - Used when creating/loading resources to indicate the total number of resources to load.
     */
    public static void initiateProgress(int maxProgg) {
        maxProgress = maxProgg;
        currentProgress = 0;
        runProgress = true;
    }

    /**
     * Increases a step in the progress.
     * Will set progress to false if we have reached the maxProgress.
     */
    public static void addProgress() {
        currentProgress++;
        if(maxProgress <= currentProgress) {
            runProgress=false;
        }
    }
    public static boolean isRunProgress() {
        return runProgress;
    }

    /**
     * Gets the proress variables as a array.
     * Clones it so that we don't reference the values where we use it.
     * @return - Int array containing the prgress values.
     */
    public static int[] getProgress() {
        return new int[]{currentProgress, maxProgress}.clone();
    }

    public static void stopProgress() {
        runProgress = false;
    }
}
