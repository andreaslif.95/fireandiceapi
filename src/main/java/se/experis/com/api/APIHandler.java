package se.experis.com.api;
import se.experis.com.exceptions.HTTPException;
import se.experis.com.model.Book;
import se.experis.com.model.Character;
import com.google.gson.Gson;
import se.experis.com.model.House;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class APIHandler {
    /**
     * Returns a character based on the id given.
     * Uses GSON to convert the JSON data to a character object.
     * @param id - The id of character.
     * @return - A character object.
     * @throws HTTPException - If any http codes except for 200 was given.
     * @throws IOException - If any read errors occured.
     */
    public static Character getCharacter(int id) throws HTTPException, IOException {
        return new Gson().fromJson(GETRequest("https://anapioficeandfire.com/api/characters/" + id), Character.class);
    }
    /**
     * Returns a house based on the id given.
     * Uses GSON to convert the JSON data to a House object.
     * @param id - The id of house.
     * @return - A house object.
     * @throws HTTPException - If any http codes except for 200 was given.
     * @throws IOException - If any read errors occured.
     */
    public static House getHouse(int id) throws HTTPException, IOException {
        return new Gson().fromJson(GETRequest("https://www.anapioficeandfire.com/api/houses/" + id), House.class);
    }
    /**
     * Returns all books present within the FIRE and ICE api.
     * Uses GSON to convert the JSON data to a Book[] object.
     * @return - A array of type Book.
     * @throws HTTPException - If any http codes except for 200 was given.
     * @throws IOException - If any read errors occured.
     */
    public static Book[] getAllBooks() throws HTTPException, IOException {
        return new Gson().fromJson(GETRequest("https://www.anapioficeandfire.com/api/books/"), Book[].class);

    }

    /**
     * A method used to send a get request the the specified URL
     * @param url - Url to which the get request should be sent.
     * @return - A string containing JSON data.
     * @throws HTTPException - If any http codes except for 200 was given.
     * @throws IOException -If any read errors occured.
     */
    private static String GETRequest(String url) throws HTTPException, IOException {
        HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
        con.setRequestMethod("GET");
        if(con.getResponseCode() == HttpURLConnection.HTTP_OK) {
            return extractJSONData(con.getInputStream());
        }
        else {
            throw new HTTPException("Error upon get request.", con.getResponseCode());
        }

    }

    /**
     * Extracts JSON data from the given inputStream
     * @param is - InputStream containing the JSON data.
     * @return - Extracted JSON data as a string
     * @throws IOException - If any read errors occur.
     */
    private static String extractJSONData(InputStream is) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));

        StringBuilder stringBuilder = new StringBuilder();

        String readLine;

        while((readLine = bufferedReader.readLine()) != null) {
            stringBuilder.append(readLine);
        }
        bufferedReader.close();
        return stringBuilder.toString();

    }
}
