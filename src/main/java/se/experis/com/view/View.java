package se.experis.com.view;

import se.experis.com.utils.Progresser;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Used to print information to the console and to fetch user input.
 */
public class View {
    public int enterCharacterNumber() throws InputMismatchException {
        System.out.print("Please enter the character id of the character you would like to view: ");
        return new Scanner(System.in).nextInt();
    }
    public int pickAnOption() throws InputMismatchException {
        cleanScreen();
        System.out.println("Pleae pick an option " +
                "\n\t1. Search for characters by id. " +
                "\n\t2. Find all pov characters in books which has the publisher Bantam Books " +
                "\n\t3. Exit the program. ");
        return new Scanner(System.in).nextInt();
    }
    public void badInputOnlyAcceptNumbers() {
        System.out.println("Bad input.. Please provide numbers.");
    }

    public void printString(String s) {
        System.out.println(s);
        System.out.print("Type any character to continue..");
        new Scanner(System.in).next();
    }

    public boolean seeSwornMembers() {
        System.out.print("Would you like to see the sworn members of this characters houses? (Y/N)");
        return new Scanner(System.in).nextLine().toLowerCase().contains("y");
    }

    /**
     * Uses the class Progress to indicate when to run and when to print.
     * @param optionalString - If a string is supposed to be printed with the progress.
     */
    public void loadingCount(String optionalString) {
        new Thread(() ->  {
            //noinspection StatementWithEmptyBody
            while(!Progresser.isRunProgress()) {
            }
            int[] progress = Progresser.getProgress();
            while(Progresser.isRunProgress()) {
                System.out.print("[" + progress[0] + "/" + progress[1] + "] loaded. " + optionalString + "\r");
                //noinspection StatementWithEmptyBody
                while(Progresser.getProgress()[0] <= progress[0]) {
                }
                progress = Progresser.getProgress();
            }
            Thread.currentThread().interrupt();
        }).start();
    }

    /**
     * Uses the class Progress to indicate when to run and when to print.
     * @param optionalString - String which is printed before the dots.
     */
    public void loadingBar(String optionalString) {
        new Thread(() ->  {
            //noinspection StatementWithEmptyBody
            while(!Progresser.isRunProgress()) {
            }
            String[] dots = {"", ".", "..", "...", "....", "....."};
            int count = 0;
            while(Progresser.isRunProgress()) {

                System.out.print("" + optionalString + dots[count] + "\r");
                try {
                    //noinspection BusyWait
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    System.out.println("Error in thread.");
                }
                if (count >= dots.length-1) {
                    count = 0;
                } else {
                    count++;
                }
            }
            Thread.currentThread().interrupt();
        }).start();
    }
    public boolean errorOccuredMessage() {
        System.out.println("\nA error occured wrong while fetching the resource would you like to try again?(Y/N)");
        return new Scanner(System.in).nextLine().toLowerCase().contains("y");
    }
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean httpErrorMesage(int statusCode) {
        System.out.println("\nStatus code ("+statusCode+").\n\tSomething went wrong while fetching the resource would you like to try again?(Y/N)");
        return new Scanner(System.in).nextLine().toLowerCase().contains("y");
    }

    public void notFound() {
        System.out.println("\nThe resource was not found, please try again.\n");
    }

    public void errorWhileFetchingCharacters(int count) {
        System.out.println("There were " + count + " errors while trying to fetch pov characters.");
    }

    /**
     * Prints empty spaces and new lines to clear the screen.
     */
    private void cleanScreen() {
        for(int i = 0; i < 400; i++) {
            System.out.println(" ");
        }
    }

}
