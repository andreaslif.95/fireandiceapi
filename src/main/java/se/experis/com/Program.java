package se.experis.com;

import se.experis.com.api.APIHandler;
import se.experis.com.controller.Controller;
import se.experis.com.view.View;

public class Program {

    public static void main(String[] args) {
        new Controller(new View()).start();
    }
}
