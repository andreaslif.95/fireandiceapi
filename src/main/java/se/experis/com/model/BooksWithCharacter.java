package se.experis.com.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Used to hold information about which books a character is a povCharacter in.
 */
public class BooksWithCharacter {
    HashMap<String, ArrayList<String>> characterInBooks;
    ArrayList<String> allBooks;

    public BooksWithCharacter() {
        this.characterInBooks = new HashMap<>();
        allBooks = new ArrayList<>();
    }

    /**
     * Adds a book to a characters arrayList. If the character is non existent within the hashmap a new posiion is created.
     * @param character - Name of the character.
     * @param book - Name of the book which the character is a povCharacter in.
     */
    public void add(String character, String book) {
        characterInBooks.computeIfAbsent(character, k -> new ArrayList<>());
        characterInBooks.get(character).add(book);
        if(!allBooks.contains(book)) {
            allBooks.add(book);
        }
    }
    public void addBook(String book) {
        if(!allBooks.contains(book)) {
            allBooks.add(book);
        }
    }
    /**
     * Builds a table with all the povCharacters and their books.
     * Format shown below.
     *                       | 1 | 2 | 3 | 4 | 5 |
     *     Jon Connington    | X |   |   |   |   |
     *     Barristan Selmy   | X |   |   |   |   |
     *      Asha Greyjoy     | X | X |   |   |   |
     *    Tyrion Lannister   | X | X | X | X |   |
     *       Sansa Stark     | X | X | X | X |   |
     *
     * @return - A table format with all characters.
     */
    public String toStringFormat() {
        StringBuilder stringBuilder = new StringBuilder();
        String tab = "                      ";
        stringBuilder.append("Books which has the publisher Bantam Books:\n");
        for(int i = 0; i < allBooks.size(); i++) {
            stringBuilder.append("\t").append(i + 1).append(".").append(allBooks.get(i)).append("\n");
        }
        stringBuilder.append("\n").append(tab).append("|");
        for(int i = 0; i < allBooks.size(); i++) {
            stringBuilder.append(" ").append(i + 1).append(" |");
        }
        stringBuilder.append("\n");
        //Iterates over the hashmap.
        for(String name : characterInBooks.keySet()) {
            int tabLength = tab.length() - name.length();
            int off = tabLength%2;
            //Calculates the number of spaces we need in order to center the name.
            //noinspection StringOperationCanBeSimplified
            stringBuilder.append(tab.substring(0, (tabLength / 2) + off)).append(name).append(tab.substring(0, tabLength / 2)).append("|");
            for(int i = 0; i < allBooks.size(); i++) {
                boolean found = false;
                try {
                    String book = characterInBooks.get(name).get(i);
                    for (String allBook : allBooks) {
                        if (book.equals(allBook)) {
                            stringBuilder.append(" X |");
                            found = true;
                            break;
                        }
                    }
                }
                catch(Exception ignored){} //Ignored since it wont affect.
                if(!found) {
                    stringBuilder.append("   |");
                }
            }
            stringBuilder.append("\n");

        }
        return stringBuilder.toString();
    }
}
