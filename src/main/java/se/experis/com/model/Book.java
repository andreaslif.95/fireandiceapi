package se.experis.com.model;

/**
 * A class to symbolize the book in the Fire and Ice api.
 */
public class Book {
    private String url;
    private String name;
    private String publisher;
    private String[] povCharacters;

    public Book(String url, String name, String publisher, String[] povCharacters) {
        this.url = url;
        this.name = name;
        this.publisher = publisher;
        this.povCharacters = povCharacters;
    }

    public String getName() {
        return name;
    }
    public String[] getPovCharacters() {
        return povCharacters;
    }

    public String getPublisher() {
        return publisher;
    }

}
