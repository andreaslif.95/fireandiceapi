package se.experis.com.model;

/**
 * A class representing a house which we get from the Fire and Ice api.
 * Used to be able to convert JSON to a Java - object with GSON.
 */
public class House {
    private String url;
    private String name;
    private String[] swornMembers;
    public House(String url, String name, String[] swornMembers) {
        this.url = url;
        this.name = name;
        this.swornMembers = swornMembers;
    }

    public String[] getSwornMembers() {
        return swornMembers.clone();
    }
    public String getName() {
        return name;
    }
}
