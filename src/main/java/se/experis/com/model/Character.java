package se.experis.com.model;

/**
 * Representing a character fromt he Fire and Ice api.
 *  * Used to be able to convert JSON to a Java - object with GSON.
 */
public class Character {
    private String url;
    private String name;
    private String gender;
    private String culture;
    private String born;
    private String died;
    private String[] titles;
    private String[] aliases;
    private String father;
    private String mother;
    private String spouse;
    private String[] allegiances;
    private String[] books;
    private String[] povBooks;
    private String[] tvSeries;
    private String[] playedBy;

    public Character(String url, String name, String gender, String culture, String born, String died, String[] titles, String[] aliases, String father, String mother, String spouse, String[] allegiances, String[] books, String[] povBooks, String[] tvSeries, String[] playedBy) {
        this.url = url;
        this.name = name;
        this.gender = gender;
        this.culture = culture;
        this.born = born;
        this.died = died;
        this.titles = titles;
        this.aliases = aliases;
        this.father = father;
        this.mother = mother;
        this.spouse = spouse;
        this.allegiances = allegiances;
        this.books = books;
        this.povBooks = povBooks;
        this.tvSeries = tvSeries;
        this.playedBy = playedBy;
    }

    public String getFirstTitle() {
        return (titles.length>0?titles[0] : "");
    }
    public String getName() {
        return name;
    }

    public String getRelevantInfo() {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Information about ").append(name).append(" \n");
        stringBuilder.append("\tGender: ").append(ifEmptyDash(gender)).append("\n");
        stringBuilder.append("\tFather: ").append(ifEmptyDash(father)).append("\n");
        stringBuilder.append("\tMother: ").append(ifEmptyDash(mother)).append("\n");
        stringBuilder.append("\tSpouse: ").append(ifEmptyDash(spouse)).append("\n");
        stringBuilder.append("\tCulture: ").append(ifEmptyDash(culture)).append("\n");
        stringBuilder.append("\tBorn: ").append(ifEmptyDash(born)).append("\n");
        stringBuilder.append("\tDied: ").append(ifEmptyDash(died)).append("\n");
        stringBuilder.append(titles.length > 0 ? "\tTitles: \n" : "");
        for(String title : titles) {
            stringBuilder.append("\t\t").append(ifEmptyDash(title)).append(" \n");
        }
        stringBuilder.append(aliases.length > 0 ? "\tAliases: \n" : "");
        for(String alias: aliases) {
            stringBuilder.append("\t\t").append(ifEmptyDash(alias)).append("\n");
        }
        return stringBuilder.toString();
    }

    /**
     * Will remove the url part of each house id any only return an int array containing the ids.
     * @return - An int array containing the ids of the houses the character belongs to.
     */
    public int[] getHouseIds() {
        int[] houseIds = new int[allegiances.length];
        for(int i = 0; i < allegiances.length; i++) {
            houseIds[i] = Integer.parseInt(allegiances[i].substring(allegiances[i].lastIndexOf("/")+1));
        }
        return houseIds;
    }
    private String ifEmptyDash(String s) {
        return (s.isEmpty() ? "-" : s);
    }

}
